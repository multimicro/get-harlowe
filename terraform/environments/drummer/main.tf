terraform {
  backend "gcs" {
    bucket  = "bucket-gtlb-edu-srvcs"
    prefix = "gtlb-edu-srvcs"
  }
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 3.0"
    }
  }
}

provider "google" {
  project = var.project
  region  = var.region
  zone    = var.zone
}

