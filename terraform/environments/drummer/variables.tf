variable "prefix" {
  default = "gtlb-edu-srvcs"
}

variable "project" {
  default = "jchoi-bda05061"
}

variable "region" {
  default = "us-west1"
}

variable "zone" {
  default = "us-west1-b"
}

variable "external_ip" {
  default = "34.133.7.112"
}

variable "GOOGLE_CREDENTIALS" {
  default = "../gtlb-edu-srvcs.json"
}
